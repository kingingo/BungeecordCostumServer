package eu.epicpvp.bungee.fakeserver.chunk;

import dev.wolveringer.bungeeutil.version.ProtocolVersion;

public class ByteChunk {
	private byte[] data;
	private byte[] biomes = new byte[265];
	private int bitmask;
	private int locX;
	private int locZ;
	private boolean biome;
	private boolean skyLight;
	private ProtocolVersion version;
	private ChunkData chunk;

	public ByteChunk(int locX, int locZ, boolean biomes, int bitmask, ProtocolVersion version, byte[] data) {
		this.locX = locX;
		this.locZ = locZ;
		this.data = data;
		this.biome = biomes;
		this.bitmask = bitmask;
		this.version = version;
	}

	public ByteChunk(int locX, int locZ, boolean biomes, int bitmask, ProtocolVersion version, byte[] data, byte[] biome) {
		this.locX = locX;
		this.locZ = locZ;
		this.data = data;
		this.biome = biomes;
		this.bitmask = bitmask;
		this.version = version;
		this.biomes = biome;
	}

	public ByteChunk(int locX, int locZ, ChunkData chunk) {
		this.locX = locX;
		this.locZ = locZ;
		this.chunk = chunk;
		this.version = chunk.getVersion();
		this.bitmask = chunk.calculateBitmask(false);
		this.biome = false;
		this.skyLight = true;
	}

	public ByteChunk() {
	}

	public int getBitmask() {
		return this.bitmask;
	}

	public byte[] getData() {
		if (this.data == null) {
			this.data = this.chunk.createData(this.bitmask, this.skyLight, this.biome);
		}
		return this.data;
	}

	public byte[] getData(ProtocolVersion version) {
		if (version == this.version) {
			if (this.data == null) {
				this.data = this.chunk.createData(this.bitmask, this.skyLight, this.biome);
			}
			return this.data;
		}
		return this.chunk.convertTo(version).createData(this.bitmask, this.skyLight, this.biome);
	}

	public int getDataSize() {
		return this.getData().length;
	}

	public int getLocX() {
		return this.locX;
	}

	public int getLocZ() {
		return this.locZ;
	}

	public ChunkData getChunk() {
		if (this.chunk == null) {
			this.chunk = ChunkImplementationRegistry.getInstance().createData(this.version, this.data, this.biomes, this.bitmask, this.skyLight, this.biome);
		}
		return this.chunk;
	}

	public boolean isBiome() {
		return this.biome;
	}

	public void setBiome(boolean biome) {
		this.biome = biome;
	}

	public ByteChunk setBitmask(int bitmask) {
		this.bitmask = bitmask;
		return this;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public void setLocX(int locX) {
		this.locX = locX;
	}

	public void setLocZ(int locZ) {
		this.locZ = locZ;
	}

	public void setSkylight(boolean skylight) {
		this.skyLight = skylight;
	}

	public boolean isSkylight() {
		return this.skyLight;
	}

	public ProtocolVersion getVersion() {
		return this.version;
	}

	public void convertTo(ProtocolVersion version) {
		if (version != this.version) {
			System.out.println("Convert not supported!");
		}
	}

	public byte[] getBiomes() {
		return this.biomes;
	}

	@Override
    public String toString() {
		return "ByteChunk [data.length=" + this.data.length + ", bitmask=" + this.bitmask + ", locX=" + this.locX + ", locZ=" + this.locZ + "]";
	}
}

