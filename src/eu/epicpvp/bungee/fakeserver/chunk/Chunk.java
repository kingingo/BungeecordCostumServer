package eu.epicpvp.bungee.fakeserver.chunk;

import java.util.ArrayList;
import java.util.HashMap;

import dev.wolveringer.api.position.BlockPosition;
import dev.wolveringer.bungeeutil.packets.Abstract.PacketPlayOut;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutUpdateSign;
import dev.wolveringer.bungeeutil.version.ProtocolVersion;
import dev.wolveringer.nbt.NBTBase;
import dev.wolveringer.nbt.NBTTagCompound;

public class Chunk {
	private ChunkData handle;
	private int locX;
	private int locY;
	private NBTTagCompound data;
	private byte[] biomes = new byte[256];
	private HashMap<BlockPosition, NBTTagCompound> blockData = new HashMap<>();

	public Chunk(NBTTagCompound data, ChunkData handle, int locX, int locY) {
		this.data = data;
		this.handle = handle;
		this.locX = locX;
		this.locY = locY;
		if (data != null && data.getCompound("Level").hasKey("Biomes")) {
			this.biomes = data.getCompound("Level").getByteArray("Biomes");
		}
		this.loadBlockData();
	}

	public Chunk(int locX, int locY, ProtocolVersion version) {
		this.locX = locX;
		this.locY = locY;
		this.handle = ChunkImplementationRegistry.getInstance().createData(version);
	}

	private void loadBlockData() {
		if (this.data != null) {
			for (NBTBase base : this.data.getCompound("Level").getList("TileEntities").asList()) {
				NBTTagCompound nbt = (NBTTagCompound) base;
				this.blockData.put(new BlockPosition(nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z")), nbt);
			}
		}
	}

	public int getLocX() {
		return this.locX;
	}

	public int getLocY() {
		return this.locY;
	}

	public ChunkData getHandle() {
		return this.handle;
	}

	public byte[] getBiomes() {
		return this.biomes;
	}

	public NBTTagCompound getBlockData(BlockPosition loc) {
		return this.blockData.get(loc);
	}

	public int getType(int x, int y, int z) {
		return this.handle.getType(x, y, z);
	}

	public void setType(int x, int y, int z, int type) {
		this.handle.setType(x, y, z, type);
	}

	public int getMetaData(int x, int y, int z) {
		return this.handle.getMetaData(x, y, z);
	}

	public void setMetaData(int x, int y, int z, int metaData) {
		this.handle.setMetaData(x, y, z, metaData);
	}

	public byte getSkyLight(int x, int y, int z) {
		return this.handle.getSkyLight(x, y, z);
	}

	public void setSkyLight(int x, int y, int z, int skyLight) {
		this.handle.setSkyLight(x, y, z, skyLight);
	}

	public byte getBlockLight(int x, int y, int z) {
		return this.handle.getBlockLight(x, y, z);
	}

	public void setBlockLight(int x, int y, int z, int blockLight) {
		this.handle.setBlockLight(x, y, z, blockLight);
	}

	public ArrayList<PacketPlayOut> getUpdatePackets() {
		ArrayList<PacketPlayOut> out = new ArrayList<>();
		for (BlockPosition pos : this.blockData.keySet()) {
			NBTTagCompound comp = this.blockData.get(pos);
			String type = comp.getString("id");
			if (!type.equalsIgnoreCase("Sign")) continue;
			out.add(new PacketPlayOutUpdateSign(pos, new String[]{comp.getString("Text1"), comp.getString("Text2"), comp.getString("Text3"), comp.getString("Text4")}));
		}
		return out;
	}
}

