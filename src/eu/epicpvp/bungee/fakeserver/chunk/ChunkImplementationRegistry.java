package eu.epicpvp.bungee.fakeserver.chunk;

import java.util.EnumMap;

import dev.wolveringer.bungeeutil.version.ProtocolVersion;
import eu.epicpvp.bungee.fakeserver.chunk.impl.v1_8.ChunkData_v1_8;
import eu.epicpvp.bungee.fakeserver.chunk.impl.v1_9.ChunkData_v1_9;

public class ChunkImplementationRegistry {

	private static ChunkImplementationRegistry instance = new ChunkImplementationRegistry();
	private EnumMap<ProtocolVersion, ConstructorInvoker> cons = new EnumMap<>(ProtocolVersion.class);

	static {
		ChunkImplementationRegistry.instance.cons.put(ProtocolVersion.v1_8, new ConstructorInvoker() {

			@Override
			public ChunkData createNewInstance() {
				return new ChunkData_v1_8();
			}

			@Override
			public ChunkData createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
				return new ChunkData_v1_8(data, bitmask, skyLight, biomes);
			}
		});
		ChunkImplementationRegistry.instance.cons.put(ProtocolVersion.v1_9, new ConstructorInvoker() {

			@Override
			public ChunkData createNewInstance() {
				return new ChunkData_v1_9();
			}

			@Override
			public ChunkData createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
				return new ChunkData_v1_9(data, bitmask, skyLight, biomes);
			}
		});
		ChunkImplementationRegistry.instance.cons.put(ProtocolVersion.v1_9_2, new ConstructorInvoker() {

			@Override
			public ChunkData createNewInstance() {
				return new ChunkData_v1_9();
			}

			@Override
			public ChunkData createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
				return new ChunkData_v1_9(data, bitmask, skyLight, biomes);
			}
		});
		ChunkImplementationRegistry.instance.cons.put(ProtocolVersion.v1_9_3, new ConstructorInvoker() {

			@Override
			public ChunkData createNewInstance() {
				return new ChunkData_v1_9();
			}

			@Override
			public ChunkData createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
				return new ChunkData_v1_9(data, bitmask, skyLight, biomes);
			}
		});
		ChunkImplementationRegistry.instance.cons.put(ProtocolVersion.v1_9_4, new ConstructorInvoker() {

			@Override
			public ChunkData createNewInstance() {
				return new ChunkData_v1_9();
			}

			@Override
			public ChunkData createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
				return new ChunkData_v1_9(data, bitmask, skyLight, biomes);
			}
		});
		ChunkImplementationRegistry.instance.cons.put(ProtocolVersion.v1_10, new ConstructorInvoker() {

			@Override
			public ChunkData createNewInstance() {
				return new ChunkData_v1_9();
			}

			@Override
			public ChunkData createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
				return new ChunkData_v1_9(data, bitmask, skyLight, biomes);
			}
		});
		ChunkImplementationRegistry.instance.cons.put(ProtocolVersion.v1_11, new ConstructorInvoker() {

			@Override
			public ChunkData createNewInstance() {
				return new ChunkData_v1_9();
			}

			@Override
			public ChunkData createNewInstance(byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
				return new ChunkData_v1_9(data, bitmask, skyLight, biomes);
			}
		});
	}

	public ChunkData createData(ProtocolVersion version) {
		if (this.cons.get(version) == null) {
			System.out.println("Can't create chunkdata for " + version);
			return null;
		}
		return this.cons.get(version).createNewInstance();
	}

	public ChunkData createData(ProtocolVersion version, byte[] data, byte[] biome, int bitmask, boolean skyLight, boolean biomes) {
		if (this.cons.get(version) == null) {
			System.out.println("Can't create chunkdata with data for " + version);
			return null;
		}
		return this.cons.get(version).createNewInstance(data, biome, bitmask, skyLight, biomes);
	}

	public static ChunkImplementationRegistry getInstance() {
		return instance;
	}

	public static void setInstance(ChunkImplementationRegistry instance) {
		ChunkImplementationRegistry.instance = instance;
	}

	private interface ConstructorInvoker {

		ChunkData createNewInstance(byte[] var1, byte[] var2, int var3, boolean var4, boolean var5);

		ChunkData createNewInstance();
	}
}

