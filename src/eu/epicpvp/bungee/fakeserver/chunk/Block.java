package eu.epicpvp.bungee.fakeserver.chunk;

import dev.wolveringer.bungeeutil.world.Material;
import dev.wolveringer.api.position.BlockPosition;
import dev.wolveringer.nbt.NBTTagCompound;

public class Block {
	private Chunk handle;
	private BlockPosition loc;
	private NBTTagCompound data;

	public Block(Chunk handle, BlockPosition loc) {
		this.handle = handle;
		this.loc = loc;
		this.data = handle.getBlockData(loc);
	}

	public Material getMaterial() {
		return Material.getMaterial((int) this.getTypeId());
	}

	public void setMaterial(Material m) {
		this.setTypeId(m.getId());
	}

	public int getTypeId() {
		return this.handle.getType((Integer) this.loc.getX() % 32, (Integer) this.loc.getY(), (Integer) this.loc.getZ() % 32);
	}

	public void setTypeId(int id) {
		this.handle.setType((Integer) this.loc.getX() % 32, (Integer) this.loc.getY(), (Integer) this.loc.getZ() % 32, id);
	}

	public byte getData() {
		return (byte) this.handle.getMetaData((Integer) this.loc.getX() % 32, (Integer) this.loc.getY(), (Integer) this.loc.getZ() % 32);
	}

	public void setData(byte data) {
		this.handle.setMetaData((Integer) this.loc.getX() % 32, (Integer) this.loc.getY(), (Integer) this.loc.getZ() % 32, data);
	}

	public NBTTagCompound getBlockData() {
		return this.data;
	}
}

