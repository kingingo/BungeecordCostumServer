package eu.epicpvp.bungee.fakeserver.chunk.impl.v1_9;

import dev.wolveringer.packet.PacketDataSerializer;

public class ChunkSelection {
	public static final int WIDTH = 16;
	public static final int HEIGHT = 16;
	public static final int DEPTH = 16;
	private BlockStorage blocks;
	private NibbleArray3d blocklight;
	private NibbleArray3d skylight;

	public ChunkSelection(boolean skylight, PacketDataSerializer in) {
		this.blocks = new BlockStorage(in);
		this.blocklight = new NibbleArray3d(in, 4096);
		if (skylight) {
			this.skylight = new NibbleArray3d(in, 4096);
		}
	}

	public ChunkSelection(boolean skylight) {
		this(new BlockStorage(), new NibbleArray3d(4096), skylight ? new NibbleArray3d(4096) : null);
	}

	public ChunkSelection(BlockStorage blocks, NibbleArray3d blocklight, NibbleArray3d skylight) {
		this.blocks = blocks;
		this.blocklight = blocklight;
		this.skylight = skylight;
	}

	public BlockStorage getBlocks() {
		return this.blocks;
	}

	public NibbleArray3d getBlockLight() {
		return this.blocklight;
	}

	public NibbleArray3d getSkyLight() {
		return this.skylight;
	}

	public boolean isEmpty() {
		return this.blocks.isEmpty();
	}

	public void writeData(PacketDataSerializer out) {
		this.blocks.writeData(out);
		this.blocklight.write(out);
		if (this.skylight != null) {
			this.skylight.write(out);
		}
	}
}

