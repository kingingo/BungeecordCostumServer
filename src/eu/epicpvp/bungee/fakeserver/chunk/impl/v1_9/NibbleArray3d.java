package eu.epicpvp.bungee.fakeserver.chunk.impl.v1_9;

import java.util.Arrays;

import dev.wolveringer.packet.PacketDataSerializer;

public class NibbleArray3d {
	private byte[] data;

	public NibbleArray3d(int size) {
		this.data = new byte[size >> 1];
	}

	public NibbleArray3d(byte[] array) {
		this.data = array;
	}

	public NibbleArray3d(PacketDataSerializer in, int size) {
		this.data = new byte[size];
		in.readBytes(size / 2);
	}

	public void write(PacketDataSerializer out) {
		out.writeBytes(this.data);
	}

	public byte[] getData() {
		return this.data;
	}

	public int get(int x, int y, int z) {
		int key = (y & 15) << 8 | z << 4 | x;
		int index = key >> 1;
		int part = key & 1;
		return part == 0 ? this.data[index] & 15 : this.data[index] >> 4 & 15;
	}

	public void set(int x, int y, int z, int val) {
		int key = (y & 15) << 8 | z << 4 | x;
		int index = key >> 1;
		int part = key & 1;
		this.data[index] = part == 0 ? (byte) (this.data[index] & 240 | val & 15) : (byte) (this.data[index] & 15 | (val & 15) << 4);
	}

	public void fill(int val) {
		int index = 0;
		while (index < this.data.length << 1) {
			int ind = index >> 1;
			int part = index & 1;
			this.data[ind] = part == 0 ? (byte) (this.data[ind] & 240 | val & 15) : (byte) (this.data[ind] & 15 | (val & 15) << 4);
			++index;
		}
	}

	@Override
	public boolean equals(Object o) {
		if (!(this == o || o instanceof NibbleArray3d && Arrays.equals(this.data, ((NibbleArray3d) o).data))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(this.data);
	}
}

