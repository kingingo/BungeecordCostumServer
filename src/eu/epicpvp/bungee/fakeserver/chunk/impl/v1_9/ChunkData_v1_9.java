package eu.epicpvp.bungee.fakeserver.chunk.impl.v1_9;

import dev.wolveringer.bungeeutil.version.ClientVersion;
import dev.wolveringer.bungeeutil.version.ProtocolVersion;
import dev.wolveringer.packet.PacketDataSerializer;
import eu.epicpvp.bungee.fakeserver.chunk.ChunkData;
import eu.epicpvp.bungee.fakeserver.chunk.ChunkImplementationRegistry;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class ChunkData_v1_9 implements ChunkData {
	private static final int MAX_SECTIONS = 16;
	private ChunkSelection[] sections = new ChunkSelection[16];
	private byte[] biomes = new byte[255];

	public ChunkData_v1_9() {
	}

	private ChunkSelection getSelection(int y, boolean create) {
		int idx = y >> 4;
		if (this.sections[idx] == null) {
			this.sections[idx] = new ChunkSelection(true);
		}
		return this.sections[idx];
	}

	public ChunkData_v1_9(byte[] data, int bitmask, boolean skyLight, boolean biomes) {
		int s = 0;
		while (s < 16) {
			if ((bitmask >> s & 1) == 1) {
				this.sections[s] = new ChunkSelection(skyLight);
			}
			++s;
		}
		PacketDataSerializer in = PacketDataSerializer.create((ByteBuf) Unpooled.wrappedBuffer((byte[]) data), (ClientVersion) ClientVersion.v1_9);
		int dy = 0;
		while (dy < 16) {
			if ((bitmask >> dy & 1) == 1) {
				this.sections[dy] = new ChunkSelection(skyLight, in);
			}
			++dy;
		}
		if (biomes) {
			in.readBytes(this.biomes);
		}
	}

	@Override
	public byte[] createData(int bitmask, boolean skyLight, boolean biome) {
		PacketDataSerializer serelizer = PacketDataSerializer.create((ByteBuf) Unpooled.buffer(), (ClientVersion) ClientVersion.v1_9);
		int dy = 0;
		while (dy < 16) {
			if ((bitmask >> dy & 1) == 1) {
				if (this.sections[dy] == null) {
					this.sections[dy] = new ChunkSelection(true);
				}
				this.sections[dy].writeData(serelizer);
			}
			++dy;
		}
		if (biome) {
			serelizer.writeBytes(this.biomes);
		}
		byte[] bout = serelizer.array();
		serelizer.release();
		return bout;
	}

	@Override
	public int getType(int x, int y, int z) {
		return this.getSelection(y, true).getBlocks().get(x, y, z).getId();
	}

	@Override
	public void setType(int x, int y, int z, int type) {
		this.getSelection(y, true).getBlocks().set(x, y, z, new BlockState(type, 0));
	}

	@Override
	public int getMetaData(int x, int y, int z) {
		return this.getSelection(y, true).getBlocks().get(x, y, z).getData();
	}

	@Override
	public void setMetaData(int x, int y, int z, int metaData) {
		this.getSelection(y, true).getBlocks().set(x, y, z, new BlockState(this.getType(x, y, z), metaData));
	}

	@Override
	public byte getSkyLight(int x, int y, int z) {
		return (byte) this.getSelection(y, true).getSkyLight().get(x, y, z);
	}

	@Override
	public void setSkyLight(int x, int y, int z, int skyLight) {
		this.getSelection(y, true).getSkyLight().set(x, y, z, skyLight);
	}

	@Override
	public byte getBlockLight(int x, int y, int z) {
		return (byte) this.getSelection(y, true).getBlockLight().get(x, y, z);
	}

	@Override
	public void setBlockLight(int x, int y, int z, int blockLight) {
		this.getSelection(y, true).getBlockLight().set(x, y, z, blockLight);
	}

	@Override
	public int calculateBitmask(boolean ignoreEmpty) {
		int mask = 0;
		int i = 0;
		while (i < this.sections.length) {
			if (this.sections[i] != null && (!this.sections[i].isEmpty() || ignoreEmpty)) {
				mask |= 1 << i;
			}
			++i;
		}
		return mask;
	}

	@Override
	public int countActiveChunks(boolean ignoreEmpty) {
		int count = 0;
		int i = 0;
		while (i < 16) {
			if (ignoreEmpty || this.sections[i] != null) {
				++count;
			}
			++i;
		}
		return count;
	}

	@Override
	public ProtocolVersion getVersion() {
		return ProtocolVersion.v1_9;
	}

	@Override
	public ChunkData convertTo(ProtocolVersion version) {
		ChunkData _new = ChunkImplementationRegistry.getInstance().createData(version);
		System.out.println("_new: " + _new);
		this.transfareBlocks(this, _new);
		return _new;
	}

	public static void main(String[] args) {
		int blockId = 12;
		int sid = 11;
		int testBitmask = 65535;
		ChunkData_v1_9 test = new ChunkData_v1_9();
		int x = 0;
		while (x < 16) {
			int y = 0;
			while (y < 256) {
				int z = 0;
				while (z < 16) {
					test.setBlockLight(x, z, y, 10);
					test.setSkyLight(x, z, y, 10);
					test.setType(x, y, z, blockId);
					test.setMetaData(x, y, z, sid);
					++z;
				}
				++y;
			}
			++x;
		}
		byte[] cash = test.convertTo(ProtocolVersion.v1_8).convertTo(ProtocolVersion.v1_9_4).createData(testBitmask, true, false);
		ChunkData_v1_9 out = new ChunkData_v1_9(cash, testBitmask, true, false);
		int right = 0;
		int wrong = 0;
		int x2 = 0;
		while (x2 < 16) {
			int y = 0;
			while (y < 256) {
				int z = 0;
				while (z < 16) {
					if (x2 % 2 != 0 && z % 2 != 0) {
						if (y % 2 == 0) {
							// empty if block
						}
						if (out.getType(x2, y, z) != blockId || out.getMetaData(x2, y, z) != sid) {
							++wrong;
						} else {
							++right;
						}
					}
					++z;
				}
				++y;
			}
			++x2;
		}
		System.out.println("Wrong: " + wrong);
		System.out.println("Right: " + right);
	}
}

