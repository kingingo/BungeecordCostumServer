package eu.epicpvp.bungee.fakeserver.chunk.impl.v1_9;

import dev.wolveringer.packet.PacketDataSerializer;

public class BlockState {
	private int id;
	private int data;

	public BlockState(int id, int data) {
		this.id = id;
		this.data = data;
	}

	public BlockState(PacketDataSerializer in) {
		int data = in.readVarInt();
		this.id = data >> 4;
		this.data = data & 15;
	}

	public void write(PacketDataSerializer serelizer) {
		if (this.id != 0) {
			serelizer.writeVarInt(this.id << 4 | this.data & 15);
		} else {
			serelizer.writeVarInt(this.id << 4 | this.data & 15);
		}
	}

	public int getId() {
		return this.id;
	}

	public int getData() {
		return this.data;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof BlockState && this.id == ((BlockState) o).id && this.data == ((BlockState) o).data) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int result = this.id;
		result = 31 * result + this.data;
		return result;
	}

	@Override
	public String toString() {
		return "BlockState [id=" + this.id + ", data=" + this.data + "]";
	}
}

