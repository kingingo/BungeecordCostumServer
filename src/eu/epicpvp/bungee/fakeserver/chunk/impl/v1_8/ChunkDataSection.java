package eu.epicpvp.bungee.fakeserver.chunk.impl.v1_8;

public class ChunkDataSection {
	public static final int WIDTH = 16;
	public static final int HEIGHT = 16;
	public static final int DEPTH = 256;
	private static final int SEC_DEPTH = 16;
	private static final int ARRAY_SIZE = 4096;
	public int nonEmptyBlockCount;
	public int tickingBlockCount;
	public char[] blockIds;
	public NibbleArray emittedLight;
	public NibbleArray skyLight;

	public ChunkDataSection(boolean skyLight) {
		this.blockIds = new char[4096];
		this.emittedLight = new NibbleArray(4096);
		if (skyLight) {
			this.skyLight = new NibbleArray(4096);
		}
	}

	public ChunkDataSection(boolean skyLight, char[] blockIds) {
		this.blockIds = blockIds;
		this.emittedLight = new NibbleArray(4096);
		if (skyLight) {
			this.skyLight = new NibbleArray(4096);
		}
		this.recalcBlockCounts();
	}

	public int getType(int dx, int dy, int dz) {
		return this.blockIds[this.index(dx, dy, dz)];
	}

	public void setType(int dx, int dy, int dz, int type) {
		int old_type = this.getType(dx, dy, dz);
		if (old_type != 0) {
			--this.nonEmptyBlockCount;
		}
		if (type != 0) {
			++this.nonEmptyBlockCount;
		}
		this.blockIds[this.index((int) dx, (int) dy, (int) dz)] = (char) type;
	}

	public boolean isEmpty() {
		if (this.nonEmptyBlockCount == 0) {
			return true;
		}
		return false;
	}

	public void setSkyLight(int dx, int dy, int dz, int value) {
		this.skyLight.setData(dx, dy, dz, value);
	}

	public int getSkyLight(int dx, int dy, int dz) {
		return this.skyLight.getData(dx, dy, dz);
	}

	public void setBlockLight(int dx, int dy, int dz, int value) {
		this.emittedLight.setData(dx, dy, dz, value);
	}

	public int getBlockLight(int dx, int dy, int dz) {
		return this.emittedLight.getData(dx, dy, dz);
	}

	public void recalcBlockCounts() {
		this.nonEmptyBlockCount = 0;
		this.tickingBlockCount = 0;
		int x = 0;
		while (x < 16) {
			int y = 0;
			while (y < 16) {
				int z = 0;
				while (z < 16) {
					int block = this.getType(x, y, z);
					if (block != 0) {
						++this.nonEmptyBlockCount;
					}
					++z;
				}
				++y;
			}
			++x;
		}
	}

	public int index(int x, int y, int z) {
		if (x < 0 || z < 0 || x >= 16 || z >= 16) {
			throw new IndexOutOfBoundsException("Coords (x=" + x + ",z=" + z + ") out of section bounds");
		}
		return (y & 15) << 8 | z << 4 | x;
	}

	public char[] getIdArray() {
		return this.blockIds;
	}

	public NibbleArray getEmittedLightArray() {
		return this.emittedLight;
	}

	public NibbleArray getSkyLightArray() {
		return this.skyLight;
	}

	public void setBlockLight(NibbleArray nibblearray) {
		this.emittedLight = nibblearray;
	}

	public void setSkyLight(NibbleArray nibblearray) {
		this.skyLight = nibblearray;
	}
}

