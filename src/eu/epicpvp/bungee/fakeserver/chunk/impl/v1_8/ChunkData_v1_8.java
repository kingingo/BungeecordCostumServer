package eu.epicpvp.bungee.fakeserver.chunk.impl.v1_8;

import dev.wolveringer.bungeeutil.version.ProtocolVersion;
import eu.epicpvp.bungee.fakeserver.chunk.ChunkData;
import eu.epicpvp.bungee.fakeserver.chunk.ChunkImplementationRegistry;

public final class ChunkData_v1_8 implements ChunkData {

	private ChunkDataSection[] sections = new ChunkDataSection[16];
	private byte[] biomes = new byte[256];

	public ChunkData_v1_8(byte[] data, int bitmask, boolean skyLight, boolean biomes) {
		int s = 0;
		while (s < 16) {
			if ((bitmask >> s & 1) == 1) {
				this.sections[s] = new ChunkDataSection(skyLight);
			}
			++s;
		}
		int pos = 0;
		int blocksLength = 4096;
		int lightLength = 2048;
		int skylightLength = skyLight ? 2048 : 0;
		int dy = 0;
		while (dy < 16) {
			if ((bitmask >> dy & 1) == 1) {
				char[] blocks = new char[blocksLength];
				int i = 0;
				while (i < blocksLength) {
					byte low = data[pos++];
					byte hight = data[pos++];
					blocks[i] = (char) ((char) (hight << 8) | low & 255);
					++i;
				}
				this.sections[dy].blockIds = blocks;
				this.sections[dy].recalcBlockCounts();
			}
			++dy;
		}
		dy = 0;
		while (dy < 16) {
			if ((bitmask >> dy & 1) == 1) {
				byte[] light = new byte[lightLength];
				System.arraycopy(data, pos, light, 0, lightLength);
				pos += lightLength;
				this.sections[dy].setBlockLight(new NibbleArray(light));
			}
			++dy;
		}
		if (skyLight) { //&& skylightLength > 0
			dy = 0;
			while (dy < 16) {
				if ((bitmask >> dy & 1) == 1) {
					byte[] skylight = new byte[skylightLength];
					//if (skylight) {
					System.arraycopy(data, pos, skylight, 0, skylightLength);
					pos += skylightLength;
					//}
					this.sections[dy].setSkyLight(new NibbleArray(skylight));
				}
				++dy;
			}
		}
		this.biomes = new byte[256];
		if (biomes) {
			System.arraycopy(data, pos, this.biomes, 0, 256);
			//if (biomes) {
			pos += 256;
			//}
		}
		if (data.length != pos) {
			System.out.println("Read to short. Avariable bytes: " + (data.length - pos));
		}
	}

	@Override
	public byte[] createData(int bitmask, boolean skyLight, boolean biome) {
		NibbleArray nibblearray;
		int position = 0;
		ChunkDataSection[] selections = this.getSelections();
		byte[] data = new byte[this.calculateChunkSize(Integer.bitCount(bitmask), skyLight, biome)];
		int dy = 0;
		while (dy < selections.length) {
			if ((bitmask >> dy & 1) == 1) {
				nibblearray = new NibbleArray(selections[dy].getIdArray());
				System.arraycopy(nibblearray.getRawData(), 0, data, position, nibblearray.getRawSize());
				position += nibblearray.getRawSize();
			}
			++dy;
		}
		dy = 0;
		while (dy < selections.length) {
			if ((bitmask >> dy & 1) == 1) {
				nibblearray = selections[dy].getEmittedLightArray();
				System.arraycopy(nibblearray.getRawData(), 0, data, position, nibblearray.getRawSize());
				position += nibblearray.getRawSize();
			}
			++dy;
		}
		if (skyLight) {
			dy = 0;
			while (dy < selections.length) {
				if ((bitmask >> dy & 1) == 1) {
					nibblearray = selections[dy].getSkyLightArray();
					System.arraycopy(nibblearray.getRawData(), 0, data, position, nibblearray.getRawSize());
					position += nibblearray.getRawSize();
				}
				++dy;
			}
		}
		if (biome) {
			int i = 0;
			while (i < 256) {
				data[position++] = this.biomes[i];
				++i;
			}
		}
		if (position != data.length) {
			System.out.println("Error. 01: " + position + ":" + data.length + " | Chunks: " + Integer.bitCount(bitmask) + " Sky: " + skyLight + ": Biome: " + biome);
		}
		return data;
	}

	public int getDataSize(int bitmask, boolean skyLight, boolean biome) {
		return this.calculateChunkSize(Integer.bitCount(bitmask), skyLight, biome);
	}

	public int calculateChunkSize(int chunks, boolean skylight, boolean biomes) {
		int chunkSize = chunks * 2 * 16 * 16 * 16;
		int blockLightSize = chunks * 16 * 16 * 16 / 2;
		int skylightSize = skylight ? chunks * 16 * 16 * 16 / 2 : 0;
		int biomeSize = biomes ? 256 : 0;
		return chunkSize + blockLightSize + skylightSize + biomeSize;
	}

	public ChunkData_v1_8() {
		int i = 0;
		while (i < this.sections.length) {
			this.sections[i] = new ChunkDataSection(true);
			++i;
		}
	}

	public ChunkDataSection[] getSelections() {
		return this.sections;
	}

	private ChunkDataSection getSection(int y) {
		int idx = y >> 4;
		if (y < 0 || y >= 256 || idx >= this.sections.length) {
			return null;
		}
		return this.sections[idx];
	}

	@Override
	public int getType(int x, int y, int z) {
		ChunkDataSection section = this.getSection(y);
		return section == null ? 0 : section.blockIds[section.index(x, y, z)] >> 4;
	}

	@Override
	public void setType(int x, int y, int z, int type) {
		if (type < 0 || type >= 256) {
			throw new IllegalArgumentException("Block type out of range: " + type);
		}
		ChunkDataSection section = this.getSection(y);
		if (section == null) {
			if (type == 0) {
				return;
			}
			int idx = y >> 4;
			if (y < 0 || y >= 256 || idx >= this.sections.length) {
				return;
			}
			this.sections[idx] = section = new ChunkDataSection(true);
		}
		int index = section.index(x, y, z);
		if (type == 0) {
			if (section.blockIds[index] != '\u0000') {
				--section.nonEmptyBlockCount;
			}
		} else if (section.blockIds[index] == '\u0000') {
			++section.nonEmptyBlockCount;
		}
		section.blockIds[index] = (char) (type << 4);
		if (type == 0 && section.nonEmptyBlockCount == 0) {
			this.sections[y / 16] = null;
			return;
		}
	}

	@Override
	public int getMetaData(int x, int y, int z) {
		ChunkDataSection section = this.getSection(y);
		return section == null ? 0 : section.blockIds[section.index(x, y, z)] & 15;
	}

	@Override
	public void setMetaData(int x, int y, int z, int metaData) {
		if (metaData < 0 || metaData >= 16) {
			throw new IllegalArgumentException("Metadata out of range: " + metaData);
		}
		ChunkDataSection section = this.getSection(y);
		if (section == null) {
			return;
		}
		int index = section.index(x, y, z);
		char type = section.blockIds[index];
		if (type == '\u0000') {
			return;
		}
		section.blockIds[index] = (char) (type & 65520 | metaData);
	}

	@Override
	public byte getSkyLight(int x, int y, int z) {
		ChunkDataSection section = this.getSection(y);
		return section == null ? 0 : section.skyLight.get(section.index(x, y, z));
	}

	@Override
	public void setSkyLight(int x, int y, int z, int skyLight) {
		ChunkDataSection section = this.getSection(y);
		if (section == null) {
			return;
		}
		section.skyLight.set(section.index(x, y, z), (byte) skyLight);
	}

	@Override
	public byte getBlockLight(int x, int y, int z) {
		ChunkDataSection section = this.getSection(y);
		return section == null ? 0 : section.emittedLight.get(section.index(x, y, z));
	}

	@Override
	public void setBlockLight(int x, int y, int z, int blockLight) {
		ChunkDataSection section = this.getSection(y);
		if (section == null) {
			return;
		}
		section.emittedLight.set(section.index(x, y, z), (byte) blockLight);
	}

	@Override
	public int calculateBitmask(boolean ignoreEmpty) {
		int mask = 0;
		int i = 0;
		while (i < this.sections.length) {
			if (this.sections[i] != null && (!this.sections[i].isEmpty() || ignoreEmpty)) {
				mask |= 1 << i;
			}
			++i;
		}
		return mask;
	}

	@Override
	public int countActiveChunks(boolean ignoreEmpty) {
		int count = 0;
		int i = 0;
		while (i < this.sections.length) {
			if (this.sections[i] != null && (!this.sections[i].isEmpty() || ignoreEmpty)) {
				++count;
			}
			++i;
		}
		return count;
	}

	@Override
	public ProtocolVersion getVersion() {
		return ProtocolVersion.v1_8;
	}

	@Override
	public ChunkData convertTo(ProtocolVersion version) {
		if (version == this.getVersion()) {
			return this;
		}
		ChunkData _new = ChunkImplementationRegistry.getInstance().createData(version);
		this.transfareBlocks(this, _new);
		return _new;
	}

	public static void main(String[] args) {
		int blockId = 55;
		int sid = 11;
		int bitmask = 65535;
		ChunkData_v1_8 test = new ChunkData_v1_8();
		int x = 0;
		while (x < 16) {
			int y = 0;
			while (y < 256) {
				int z = 0;
				while (z < 16) {
					test.setType(x, y, z, blockId);
					test.setMetaData(x, y, z, sid);
					++z;
				}
				++y;
			}
			++x;
		}
		byte[] cash = test.createData(bitmask, true, false);
		ChunkData_v1_8 out = new ChunkData_v1_8(cash, bitmask, true, false);
		int right = 0;
		int wrong = 0;
		int x2 = 0;
		while (x2 < 16) {
			int y = 0;
			while (y < 256) {
				int z = 0;
				while (z < 16) {
					if (x2 % 2 != 0 && z % 2 != 0) {
						if (y % 2 == 0) {
							// empty if block
						}
						if (out.getType(x2, y, z) != blockId || out.getMetaData(x2, y, z) != sid) {
							++wrong;
							System.out.println(out.getType(x2, y, z));
						} else {
							++right;
						}
					}
					++z;
				}
				++y;
			}
			++x2;
		}
		System.out.println("Wrong: " + wrong);
		System.out.println("Right: " + right);
	}
}

