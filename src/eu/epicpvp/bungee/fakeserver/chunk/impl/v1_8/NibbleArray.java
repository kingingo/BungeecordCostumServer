package eu.epicpvp.bungee.fakeserver.chunk.impl.v1_8;

import java.util.Arrays;

public final class NibbleArray {
	private final byte[] data;

	public NibbleArray(int size) {
		if (size < 0 || size % 2 != 0) {
			throw new IllegalArgumentException("size must be a positive even number, not " + size);
		}
		this.data = new byte[size / 2];
	}

	public NibbleArray(byte[] data) {
		this.data = data;
	}

	public NibbleArray(char[] idArray) {
		this.data = new byte[idArray.length * 2];
		int i = 0;
		int n = idArray.length;
		int n2 = 0;
		while (n2 < n) {
			char c = idArray[n2];
			this.data[i++] = (byte) ((byte) c & 255);
			this.data[i++] = (byte) ((byte) (c >> 8) & 255);
			++n2;
		}
	}

	public int size() {
		return 2 * this.data.length;
	}

	public int getRawSize() {
		return this.data.length;
	}

	public int byteSize() {
		return this.data.length;
	}

	public byte get(int index) {
		byte val = this.data[index / 2];
		if (index % 2 == 0) {
			return (byte) (val & 15);
		}
		return (byte) ((val & 240) >> 4);
	}

	public void set(int index, byte value) {
		value = (byte) (value & 15);
		int half = index / 2;
		byte previous = this.data[half];
		this.data[half] = index % 2 == 0 ? (byte) (previous & 240 | value) : (byte) (previous & 15 | value << 4);
	}

	public void fill(byte value) {
		value = (byte) (value & 15);
		Arrays.fill(this.data, (byte) (value << 4 | value));
	}

	public byte[] getRawData() {
		return this.data;
	}

	public void setRawData(byte[] source) {
		if (source.length != this.data.length) {
			throw new IllegalArgumentException("expected byte array of length " + this.data.length + ", not " + source.length);
		}
		System.arraycopy(source, 0, this.data, 0, source.length);
	}

	public NibbleArray snapshot() {
		return new NibbleArray(this.data.clone());
	}

	public int getData(int x, int y, int z) {
		return this.get(NibbleArray.getCompressedData(x, y, z));
	}

	public void setData(int x, int y, int z, int data) {
		this.set(NibbleArray.getCompressedData(x, y, z), (byte) data);
	}

	private static int getCompressedData(int x, int y, int z) {
		return y << 8 | z << 4 | x;
	}

	public static void main(String[] args) {
		System.out.println(NibbleArray.getCompressedData(0, 8, 0));
	}
}

