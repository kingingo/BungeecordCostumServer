package eu.epicpvp.bungee.fakeserver.chunk;

import dev.wolveringer.bungeeutil.version.ProtocolVersion;

public interface ChunkData {
	public static final int WIDTH = 16;
	public static final int HEIGHT = 16;
	public static final int DEPTH = 256;
	public static final int SEC_DEPTH = 16;

	public int getType(int var1, int var2, int var3);

	public void setType(int var1, int var2, int var3, int var4);

	public int getMetaData(int var1, int var2, int var3);

	public void setMetaData(int var1, int var2, int var3, int var4);

	public byte getSkyLight(int var1, int var2, int var3);

	public void setSkyLight(int var1, int var2, int var3, int var4);

	public byte getBlockLight(int var1, int var2, int var3);

	public void setBlockLight(int var1, int var2, int var3, int var4);

	public int calculateBitmask(boolean var1);

	public int countActiveChunks(boolean var1);

	public byte[] createData(int var1, boolean var2, boolean var3);

	public ProtocolVersion getVersion();

	public ChunkData convertTo(ProtocolVersion var1);

	default public void transfareBlocks(ChunkData a, ChunkData b) {
		if (a == null || b == null) {
			throw new NullPointerException("a = " + a + " b = " + b);
		}
		int x = 0;
		while (x < 16) {
			int y = 0;
			while (y < 256) {
				int z = 0;
				while (z < 16) {
					b.setType(x, y, z, a.getType(x, y, z));
					b.setMetaData(x, y, z, a.getMetaData(x, y, z));
					b.setBlockLight(x, y, z, a.getBlockLight(x, y, z));
					b.setSkyLight(x, y, z, a.getSkyLight(x, y, z));
					++z;
				}
				++y;
			}
			++x;
		}
	}
}

