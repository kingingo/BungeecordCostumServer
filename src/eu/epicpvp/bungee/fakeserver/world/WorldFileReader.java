package eu.epicpvp.bungee.fakeserver.world;

import java.io.File;
import java.util.HashMap;

public class WorldFileReader {
	private static HashMap<File, World> worlds = new HashMap<>();

	public static World read(File file) {
		if (worlds.containsKey(file)) {
			return worlds.get(file);
		}
		if (!file.exists()) {
			throw new RuntimeException("World (" + file.getAbsolutePath() + ") not exist");
		}
		String[] list = file.list((dir, name) -> name.equalsIgnoreCase("level.dat"));
		if (list == null || list.length == 0) {
			throw new NullPointerException(file.getAbsolutePath() + " isnt a world dir.");
		}
		World manager = new World(new File(file, "level.dat"));
		File[] arrfile = new File(file, "region/").listFiles((dir, name) -> name.endsWith(".mca"));
		if (arrfile == null) {
			throw new NullPointerException("arrfile");
		}
		int n = arrfile.length;
		int n2 = 0;
		while (n2 < n) {
			File nbtFile = arrfile[n2];
			try {
				MCAFileReader reader = new MCAFileReader(nbtFile);
				manager.getChunks().addChunkFile(reader);
			} catch (Exception e) {
				e.printStackTrace();
			}
			++n2;
		}
		worlds.put(file, manager);
		return manager;
	}

	public static boolean isWorld(File file) {
		if (worlds.containsKey(file)) {
			return true;
		}
		if (!file.exists()) {
			return false;
		}
		String[] list = file.list((dir, name) -> name.equalsIgnoreCase("level.dat"));
		if (list == null || list.length == 0) {
			return false;
		}
		File[] files = new File(file, "region/").listFiles((dir, name) -> name.endsWith(".mca"));
		if (files == null || files.length == 0) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(WorldFileReader.read(new File("/home/wolverindev/.minecraft/saves/alina/")).getBlock(0, 0, 0).getTypeId());
	}
}

