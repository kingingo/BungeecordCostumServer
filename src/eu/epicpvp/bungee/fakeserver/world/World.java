package eu.epicpvp.bungee.fakeserver.world;

import java.io.File;
import java.util.ArrayList;

import dev.wolveringer.api.position.BlockPosition;
import dev.wolveringer.api.position.Location;
import dev.wolveringer.nbt.NBTTagCompound;
import eu.epicpvp.bungee.fakeserver.chunk.Block;
import eu.epicpvp.bungee.fakeserver.chunk.Chunk;

public class World {
	private ChunkManager chunks;
	private LevelReader prop;

	public World(File level) {
		this.prop = new LevelReader(level);
		this.chunks = new ChunkManager();
	}

	public ChunkManager getChunks() {
		return this.chunks;
	}

	public NBTTagCompound getRoot() {
		return this.prop.getRoot();
	}

	public Location getWorldSpawn() {
		return this.prop.getWorldSpawn();
	}

	public long getWorldTime() {
		return this.prop.getWorldTime();
	}

	public boolean isRaining() {
		return this.prop.isRaining();
	}

	public boolean isThundering() {
		return this.prop.isThundering();
	}

	@Override
	public int hashCode() {
		return this.prop.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return this.prop.equals(obj);
	}

	@Override
	public String toString() {
		return this.prop.toString();
	}

	public ArrayList<Chunk> getChunks(int x, int z, int r) {
		return this.chunks.getChunks(x, z, r);
	}

	public Chunk getChuk(int chunkX, int chunkZ) {
		return this.chunks.getChuk(chunkX, chunkZ);
	}

	public Block getBlock(int x, int y, int z) {
		return new Block(this.chunks.getChuk(x >> 4, z >> 4), new BlockPosition(x, y, z));
	}
}

