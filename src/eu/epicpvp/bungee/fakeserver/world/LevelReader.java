package eu.epicpvp.bungee.fakeserver.world;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import dev.wolveringer.api.position.Location;
import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTTagCompound;

public class LevelReader {
	private NBTTagCompound root;

	public LevelReader(File file) {
		try {
			this.root = NBTCompressedStreamTools.read((InputStream) new FileInputStream(file)).getCompound("Data");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public NBTTagCompound getRoot() {
		return this.root;
	}

	public Location getWorldSpawn() {
		return new Location((double) this.root.getInt("SpawnX"), (double) this.root.getInt("SpawnY"), (double) this.root.getInt("SpawnZ"));
	}

	public long getWorldTime() {
		return this.root.getLong("DayTime");
	}

	public boolean isRaining() {
		return this.root.getBoolean("raining");
	}

	public boolean isThundering() {
		return this.root.getBoolean("thundering");
	}
}

