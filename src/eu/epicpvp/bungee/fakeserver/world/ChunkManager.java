package eu.epicpvp.bungee.fakeserver.world;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import dev.wolveringer.nbt.NBTBase;
import dev.wolveringer.nbt.NBTCompressedStreamTools;
import dev.wolveringer.nbt.NBTReadLimiter;
import dev.wolveringer.nbt.NBTTagCompound;
import eu.epicpvp.bungee.fakeserver.chunk.Chunk;
import eu.epicpvp.bungee.fakeserver.chunk.impl.v1_8.ChunkDataSection;
import eu.epicpvp.bungee.fakeserver.chunk.impl.v1_8.ChunkData_v1_8;
import eu.epicpvp.bungee.fakeserver.chunk.impl.v1_8.NibbleArray;

public class ChunkManager {
	private HashMap<Location2DKey, MCAFileReader> superChunks = new HashMap();
	private HashMap<Location2DKey, Chunk> chunks = new HashMap();

	public ArrayList<Chunk> getChunks(int x, int z, int r) {
		ArrayList<Chunk> out = new ArrayList<>();
		int dx = -r;
		while (dx < r) {
			int dz = -r;
			while (dz < r) {
				out.add(this.getChuk(x + dx, z + dz));
				++dz;
			}
			++dx;
		}
		return out;
	}

	public Chunk getChuk(int chunkX, int chunkZ) {
		Chunk data = this.chunks.get(new Location2DKey(chunkX, chunkZ));
		if (data == null) {
			byte[] byteData;
			MCAFileReader file = this.getFile(chunkX, chunkZ);
			if (file != null && (byteData = file.getChunkData(chunkX & 31, chunkZ & 31)) != null) {
				data = this.createChunk(byteData);
			}
			if (data != null) {
				this.chunks.put(new Location2DKey(chunkX, chunkZ), data);
			}
		}
		if (data == null) {
			data = new Chunk(null, null, chunkX, chunkZ);
		}
		return data;
	}

	private Chunk createChunk(byte[] data) {
		NBTTagCompound comp;
		ChunkData_v1_8 chunk = new ChunkData_v1_8();
		try {
			comp = NBTCompressedStreamTools.read((DataInput) new DataInputStream(new ByteArrayInputStream(data)), (NBTReadLimiter) NBTReadLimiter.unlimited);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		int chunkX = comp.getCompound("Level").getInt("xPos");
		int chunkZ = comp.getCompound("Level").getInt("zPos");
		for (NBTBase base_selection : comp.getCompound("Level").getList("Sections").asList()) {
			NBTTagCompound selection = (NBTTagCompound) base_selection;
			byte y = selection.getByte("Y");
			ChunkDataSection chunkDataSelection = new ChunkDataSection(true);
			byte[] blockIds = selection.getByteArray("Blocks");
			NibbleArray blockData = new NibbleArray(selection.getByteArray("Data"));
			NibbleArray skyLight = new NibbleArray(selection.getByteArray("SkyLight"));
			NibbleArray blockLight = new NibbleArray(selection.getByteArray("BlockLight"));
			char[] blocks = new char[4096];
			int i = 0;
			while (i < blocks.length) {
				blocks[i] = (char) ((blockIds[i] & 255) << 4 | blockData.get(i) & 15);
				++i;
			}
			chunkDataSelection.blockIds = blocks;
			chunkDataSelection.skyLight = skyLight;
			chunkDataSelection.emittedLight = blockLight;
			chunkDataSelection.recalcBlockCounts();
			chunk.getSelections()[y] = chunkDataSelection;
		}
		return new Chunk(comp, chunk, chunkX, chunkZ);
	}

	private MCAFileReader getFile(int chunkX, int chunkZ) {
		return this.superChunks.get(new Location2DKey(chunkX >> 5, chunkZ >> 5));
	}

	public void addChunkFile(MCAFileReader file) {
		this.superChunks.put(new Location2DKey(file.getLocX(), file.getLocZ()), file);
	}

	public static void main(String[] args) {
		System.out.println(Integer.toBinaryString(3984));
	}

	private static class Location2DKey {
		private int x;
		private int z;

		public Location2DKey(int x, int z) {
			this.x = x;
			this.z = z;
		}

		public Location2DKey setX(int x) {
			this.x = x;
			return this;
		}

		public int getX() {
			return this.x;
		}

		public Location2DKey setZ(int z) {
			this.z = z;
			return this;
		}

		public int getZ() {
			return this.z;
		}

		@Override
		public int hashCode() {
			int prime = 31;
			int result = 1;
			result = 31 * result + this.x;
			result = 31 * result + this.z;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (this.getClass() != obj.getClass()) {
				return false;
			}
			Location2DKey other = (Location2DKey) obj;
			if (this.x != other.x) {
				return false;
			}
			if (this.z != other.z) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return "Location2DKey [x=" + this.x + ", z=" + this.z + "]";
		}
	}
}

