package eu.epicpvp.bungee.fakeserver.world;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public class MCAFileReader {
	private static final int VERSION_GZIP = 1;
	private static final int VERSION_DEFLATE = 2;
	private static final int SECTOR_BYTES = 4096;
	private static final int SECTOR_INTS = 1024;
	static final int CHUNK_HEADER_SIZE = 5;
	private static final byte[] emptySector = new byte[4096];
	private final File fileName;
	private RandomAccessFile file;
	private final int[] offsets = new int[1024];
	private final int[] chunkTimestamps = new int[1024];
	private ArrayList<Boolean> sectorFree;
	private int sizeDelta;
	private long lastModified = 0;
	private int locX;
	private int locZ;

	public MCAFileReader(File path) {
		this.fileName = path;
		this.debugln("REGION LOAD " + this.fileName);
		this.sizeDelta = 0;
		try {
			int i;
			if (path.exists()) {
				this.lastModified = path.lastModified();
			}
			this.file = new RandomAccessFile(path, "rw");
			if (this.file.length() < 4096) {
				i = 0;
				while (i < 1024) {
					this.file.writeInt(0);
					++i;
				}
				i = 0;
				while (i < 1024) {
					this.file.writeInt(0);
					++i;
				}
				this.sizeDelta += 8192;
			}
			if ((this.file.length() & 4095) != 0) {
				i = 0;
				while ((long) i < (this.file.length() & 4095)) {
					this.file.write(0);
					++i;
				}
			}
			int nSectors = (int) this.file.length() / 4096;
			this.sectorFree = new ArrayList(nSectors);
			int i2 = 0;
			while (i2 < nSectors) {
				this.sectorFree.add(true);
				++i2;
			}
			this.sectorFree.set(0, false);
			this.sectorFree.set(1, false);
			this.file.seek(0);
			i2 = 0;
			while (i2 < 1024) {
				int offset;
				this.offsets[i2] = offset = this.file.readInt();
				if (offset != 0 && (offset >> 8) + (offset & 255) <= this.sectorFree.size()) {
					int sectorNum = 0;
					while (sectorNum < (offset & 255)) {
						this.sectorFree.set((offset >> 8) + sectorNum, false);
						++sectorNum;
					}
				}
				++i2;
			}
			i2 = 0;
			while (i2 < 1024) {
				int lastModValue;
				this.chunkTimestamps[i2] = lastModValue = this.file.readInt();
				++i2;
			}
			String[] cords = path.getName().replaceAll(".mca", "").replaceAll("r.", "").split("\\.");
			this.locX = Integer.parseInt(cords[0]);
			this.locZ = Integer.parseInt(cords[1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public long lastModified() {
		return this.lastModified;
	}

	public synchronized int getSizeDelta() {
		int ret = this.sizeDelta;
		this.sizeDelta = 0;
		return ret;
	}

	private void debug(String in) {
	}

	private void debugln(String in) {
		this.debug(String.valueOf(in) + "\n");
	}

	private void debug(String mode, int x, int z, String in) {
		this.debug("REGION " + mode + " " + this.fileName.getName() + "[" + x + "," + z + "] = " + in);
	}

	private void debug(String mode, int x, int z, int count, String in) {
		this.debug("REGION " + mode + " " + this.fileName.getName() + "[" + x + "," + z + "] " + count + "B = " + in);
	}

	private void debugln(String mode, int x, int z, String in) {
		this.debug(mode, x, z, String.valueOf(in) + "\n");
	}

	public synchronized DataInputStream getChunkDataInputStream(int x, int z) {
		try {
			int length;
			block10:
			{
				int sectorNumber;
				int numSectors;
				block9:
				{
					int offset;
					block8:
					{
						if (this.outOfBounds(x, z)) {
							this.debugln("READ", x, z, "out of bounds");
							return null;
						}
						offset = this.getOffset(x, z);
						if (offset != 0)
							break block8;
						return null;
					}
					sectorNumber = offset >> 8;
					numSectors = offset & 255;
					if (sectorNumber + numSectors <= this.sectorFree.size())
						break block9;
					this.debugln("READ", x, z, "invalid sector");
					return null;
				}
				this.file.seek(sectorNumber * 4096);
				length = this.file.readInt();
				if (length <= 4096 * numSectors)
					break block10;
				this.debugln("READ", x, z, "invalid length: " + length + " > 4096 * " + numSectors);
				return null;
			}
			byte version = this.file.readByte();
			if (version == 1) {
				byte[] data = new byte[length - 1];
				this.file.read(data);
				DataInputStream ret = new DataInputStream(new GZIPInputStream(new ByteArrayInputStream(data)));
				return ret;
			}
			if (version == 2) {
				byte[] data = new byte[length - 1];
				this.file.read(data);
				InflaterInputStream in = new InflaterInputStream(new ByteArrayInputStream(data));
				DataInputStream ret = new DataInputStream(in);
				return ret;
			}
			this.debugln("READ", x, z, "unknown version " + version);
		} catch (IOException e) {
			this.debugln("READ", x, z, "exception");
			return null;
		}
		return null;
	}

	public synchronized byte[] getChunkData(int x, int z) {
		DataInputStream in = this.getChunkDataInputStream(x, z);
		if (in != null) {
			try {
				int bufferSize = 2048;
				byte[] buffer = new byte[bufferSize];
				byte[] out = new byte[]{};
				while (in.available() > 0) {
					int size = 0;
					size = 0;
					while (size < bufferSize && in.available() > 0) {
						buffer[size] = (byte) in.read();
						++size;
					}
					if (size < bufferSize && size != -1) {
						System.arraycopy(buffer, 0, buffer, 0, size);
					}
					out = this.concatenate(out, buffer);
					buffer = new byte[bufferSize];
				}
				return out;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private byte[] concatenate(byte[] out, byte[] buffer) {
		int aLen = out.length;
		int bLen = buffer.length;
		byte[] c = new byte[aLen + bLen];
		System.arraycopy(out, 0, c, 0, aLen);
		System.arraycopy(buffer, 0, c, aLen, bLen);
		return c;
	}

	public DataOutputStream getChunkDataOutputStream(int x, int z) {
		if (this.outOfBounds(x, z)) {
			return null;
		}
		return new DataOutputStream(new DeflaterOutputStream(new ChunkBuffer(x, z)));
	}

	protected synchronized void write(int x, int z, byte[] data, int length) {
		try {
			int offset = this.getOffset(x, z);
			int sectorNumber = offset >> 8;
			int sectorsAllocated = offset & 255;
			int sectorsNeeded = (length + 5) / 4096 + 1;
			if (sectorsNeeded >= 256) {
				return;
			}
			if (sectorNumber != 0 && sectorsAllocated == sectorsNeeded) {
				this.debug("SAVE", x, z, length, "rewrite");
				this.write(sectorNumber, data, length);
			} else {
				int i;
				int i2 = 0;
				while (i2 < sectorsAllocated) {
					this.sectorFree.set(sectorNumber + i2, true);
					++i2;
				}
				int runStart = this.sectorFree.indexOf(true);
				int runLength = 0;
				if (runStart != -1) {
					i = runStart;
					while (i < this.sectorFree.size()) {
						if (runLength != 0) {
							runLength = this.sectorFree.get(i).booleanValue() ? ++runLength : 0;
						} else if (this.sectorFree.get(i).booleanValue()) {
							runStart = i;
							runLength = 1;
						}
						if (runLength >= sectorsNeeded)
							break;
						++i;
					}
				}
				if (runLength >= sectorsNeeded) {
					this.debug("SAVE", x, z, length, "reuse");
					sectorNumber = runStart;
					this.setOffset(x, z, sectorNumber << 8 | sectorsNeeded);
					i = 0;
					while (i < sectorsNeeded) {
						this.sectorFree.set(sectorNumber + i, false);
						++i;
					}
					this.write(sectorNumber, data, length);
				} else {
					this.debug("SAVE", x, z, length, "grow");
					this.file.seek(this.file.length());
					sectorNumber = this.sectorFree.size();
					i = 0;
					while (i < sectorsNeeded) {
						this.file.write(emptySector);
						this.sectorFree.add(false);
						++i;
					}
					this.sizeDelta += 4096 * sectorsNeeded;
					this.write(sectorNumber, data, length);
					this.setOffset(x, z, sectorNumber << 8 | sectorsNeeded);
				}
			}
			this.setTimestamp(x, z, (int) (System.currentTimeMillis() / 1000));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getLocX() {
		return this.locX;
	}

	public int getLocZ() {
		return this.locZ;
	}

	private void write(int sectorNumber, byte[] data, int length) throws IOException {
		this.debugln(" " + sectorNumber);
		this.file.seek(sectorNumber * 4096);
		this.file.writeInt(length + 1);
		this.file.writeByte(2);
		this.file.write(data, 0, length);
	}

	private boolean outOfBounds(int x, int z) {
		if (x >= 0 && x < 32 && z >= 0 && z < 32) {
			return false;
		}
		return true;
	}

	private int getOffset(int x, int z) {
		return this.offsets[x + z * 32];
	}

	public boolean hasChunk(int x, int z) {
		if (this.getOffset(x, z) != 0) {
			return true;
		}
		return false;
	}

	private void setOffset(int x, int z, int offset) throws IOException {
		this.offsets[x + z * 32] = offset;
		this.file.seek((x + z * 32) * 4);
		this.file.writeInt(offset);
	}

	private void setTimestamp(int x, int z, int value) throws IOException {
		this.chunkTimestamps[x + z * 32] = value;
		this.file.seek(4096 + (x + z * 32) * 4);
		this.file.writeInt(value);
	}

	public void close() throws IOException {
		this.file.close();
	}

	class ChunkBuffer extends ByteArrayOutputStream {
		private int x;
		private int z;

		public ChunkBuffer(int x, int z) {
			super(8096);
			this.x = x;
			this.z = z;
		}

		@Override
		public void close() {
			MCAFileReader.this.write(this.x, this.z, this.buf, this.count);
		}
	}
}
