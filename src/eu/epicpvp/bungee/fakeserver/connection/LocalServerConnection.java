package eu.epicpvp.bungee.fakeserver.connection;

import java.net.InetSocketAddress;

import dev.wolveringer.api.position.BlockPosition;
import dev.wolveringer.bungeeutil.entity.player.Player;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutChat;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutEntityProperties;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutKeepAlive;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutPosition;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutSetExperience;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutSpawnPostition;
import dev.wolveringer.bungeeutil.packets.PacketPlayOutUpdateHealth;
import dev.wolveringer.bungeeutil.version.ProtocolVersion;
import dev.wolveringer.network.IInitialHandler;
import eu.epicpvp.bungee.fakeserver.ServerConfiguration;
import eu.epicpvp.bungee.fakeserver.bungee.AbstractChannelHandlerContext;
import eu.epicpvp.bungee.fakeserver.bungee.KeepAliveServerInfo;
import eu.epicpvp.bungee.fakeserver.chunk.ByteChunk;
import eu.epicpvp.bungee.fakeserver.chunk.Chunk;
import eu.epicpvp.bungee.fakeserver.chunk.ChunkData;
import eu.epicpvp.bungee.fakeserver.packets.PacketPlayOutMapChunk;
import eu.epicpvp.bungee.fakeserver.world.World;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.BungeeServerInfo;
import net.md_5.bungee.PacketConstants;
import net.md_5.bungee.ServerConnection;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.score.Objective;
import net.md_5.bungee.api.score.Scoreboard;
import net.md_5.bungee.netty.ChannelWrapper;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.packet.Login;
import net.md_5.bungee.protocol.packet.PluginMessage;
import net.md_5.bungee.protocol.packet.Respawn;
import net.md_5.bungee.protocol.packet.ScoreboardObjective;
import net.md_5.bungee.protocol.packet.Team;

public abstract class LocalServerConnection extends ServerConnection {
	private static final Chunk EMPTY_CHUNK = new Chunk(0, 0, ProtocolVersion.v1_8);
	private static final Unsafe UNSAFE = new Unsafe() {
		@Override
		public void sendPacket(DefinedPacket packet) {
		}
	};
	private UserConnection player;
	private boolean active = true;
	private ServerConfiguration cfg;
	private ServerInfo last;
	private Plugin pluginInstance;
	private PacketPlayOutChat actionbarPacket;
	private ChannelWrapper channel;

	public LocalServerConnection(Plugin pluginInstance, UserConnection player, ServerConfiguration sc) {
		super(null, null);
		this.channel = new ChannelWrapper(new AbstractChannelHandlerContext()) {

			@Override
			public void write(Object packet) {
			}

			@Override
			public void close() {
				LocalServerConnection.this.inactive();
			}
		};
		this.pluginInstance = pluginInstance;
		this.player = player;
		ServerConnection old = player.getServer();
		this.player.setServer(this);
		if (old != null) {
			if (old instanceof LocalServerConnection) {
				((LocalServerConnection) old).inactive();
			} else {
				this.last = old.getInfo();
				old.setObsolete(true);
				old.disconnect("");
			}
		}
		this.cfg = sc;
		this.updateActrionBar();
		BungeeCord.getInstance().getScheduler().runAsync(pluginInstance, new Runnable() {
			int id;

			@Override
			public void run() {
				while (LocalServerConnection.this.active && ((IInitialHandler) player.getPendingConnection()).isConnected) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (actionbarPacket != null)
						((Player) (ProxiedPlayer) LocalServerConnection.this.player).sendPacket(LocalServerConnection.this.actionbarPacket);
					PacketPlayOutKeepAlive p = new PacketPlayOutKeepAlive();
					p.setId(this.id++);
					LocalServerConnection.this.getPlayer().sendPacket(p);
				}
			}
		});
		this.joinServer(this.last != null);
		this.updateTab();
		this.sendSetMessages();
		this.getInfo().addPlayer(this.player);
	}

	public void updateActrionBar() {
		if (this.cfg.getActionBar() == null)
			return;
		this.actionbarPacket = new PacketPlayOutChat();
		this.actionbarPacket.setModus((byte) 2);
		this.actionbarPacket.setRawMessage(("{\"text\":\"" + this.cfg.getActionBar() + "\"}").getBytes());
	}

	public void updateTab() {
		ComponentBuilder builder = new ComponentBuilder("");
		int i = 1;
		for (String s : this.cfg.getTab()) {
			if (i + 1 < this.cfg.getTab().size()) {
				builder.append(s + "\n");
				continue;
			}
			if (i++ >= this.cfg.getTab().size())
				continue;
			builder.append(s);
		}
		this.player.setTabHeader(builder.create(), new ComponentBuilder(this.cfg.getTab().get(this.cfg.getTab().size() - 1)).create());
	}

	public void updateTitle() {
		BungeeCord.getInstance().createTitle().title(new ComponentBuilder(this.cfg.getTitle()).create()).subTitle(new ComponentBuilder(this.cfg.getSubTitle()).create()).fadeIn(0).stay(60 * 20).fadeOut(20).send(this.player);
	}

	private void sendSetMessages() {
		for (String s : this.cfg.getChat()) {
			this.player.sendMessage(s);
		}
	}

	public Player getPlayer() {
		return (Player) (ProxiedPlayer) this.player;
	}

	private void joinServer(boolean flag) {
		final World manager = this.cfg.getWorld();
		if (flag) {
			this.player.getTabListHandler().onServerChange();
			Scoreboard serverScoreboard = this.player.getServerSentScoreboard();
			for (Objective objective : serverScoreboard.getObjectives()) {
				this.player.unsafe().sendPacket(new ScoreboardObjective(objective.getName(), objective.getValue(), "integer", (byte) 1));
			}
			for (net.md_5.bungee.api.score.Team team : serverScoreboard.getTeams()) {
				this.player.unsafe().sendPacket(new Team(team.getName()));
			}
			serverScoreboard.clear();
			try {
				this.player.unsafe().sendPacket(PacketConstants.DIM1_SWITCH);
				this.player.unsafe().sendPacket(PacketConstants.DIM2_SWITCH);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			this.player.unsafe().sendPacket(new Respawn(0, (short) 0, (short) 0, "world"));
			this.player.setDimensionChange(false);
			this.player.setServerEntityId(1);
		} else {
			this.player.setClientEntityId(1);
			this.player.setServerEntityId(1);
			Login modLogin = new Login(1, (short) 0, 0, (short) 1, (short) ((byte) this.player.getPendingConnection().getListener().getTabListSize()), "Default", false);
			this.player.unsafe().sendPacket(modLogin);
			ByteBuf brand = ByteBufAllocator.DEFAULT.heapBuffer();
			DefinedPacket.writeString(BungeeCord.getInstance().getName() + " (" + BungeeCord.getInstance().getVersion() + ")", brand);
			this.player.unsafe().sendPacket(new PluginMessage("MC|Brand", brand.array().clone(), false));
			brand.release();
			this.getPlayer().sendPacket(new PacketPlayOutSpawnPostition(new BlockPosition(manager.getWorldSpawn().getBlockX(), manager.getWorldSpawn().getBlockY(), manager.getWorldSpawn().getBlockZ())));
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.getPlayer().sendPacket(new PacketPlayOutUpdateHealth(1.0f, 20, 0.0f));
		this.getPlayer().sendPacket(new PacketPlayOutEntityProperties().addProperty(new PacketPlayOutEntityProperties.EntityProperty("generic.maxHealth", 0.0)).setEntityId(this.player.getClientEntityId()));
		this.getPlayer().sendPacket(new PacketPlayOutSetExperience().setExp(0.0f).setLevel(-10).setTotal(11111));
		this.getPlayer().sendPacket(new PacketPlayOutPosition(manager.getWorldSpawn(), true));
		BungeeCord.getInstance().getScheduler().runAsync(this.pluginInstance, () -> LocalServerConnection.this.sendWorldChunks(manager));
	}

	public void sendWorldChunks(World manager) {
		for (Chunk data : manager.getChunks(manager.getWorldSpawn().getBlockX() >> 4, manager.getWorldSpawn().getBlockZ() >> 4, this.cfg.getRenderDistance())) {
			ChunkData cdata = data.getHandle() == null ? EMPTY_CHUNK.getHandle() : data.getHandle();
			ByteChunk c = new ByteChunk(data.getLocX(), data.getLocY(), cdata).setBitmask(cdata.calculateBitmask(true));
			PacketPlayOutMapChunk p = new PacketPlayOutMapChunk();
			p.setBuffer(null);
			p.setSelection(c);
			this.getPlayer().sendPacket(p);
		}
		this.getPlayer().sendPacket(new PacketPlayOutPosition(manager.getWorldSpawn(), true));
	}

	@Override
	public InetSocketAddress getAddress() {
		return new InetSocketAddress(25565);
	}

	@Override
	public void disconnect(String paramString) {
		this.inactive();
	}

	@Override
	public void disconnect(BaseComponent... paramArrayOfBaseComponent) {
		this.inactive();
	}

	@Override
	public void disconnect(BaseComponent paramBaseComponent) {
		this.inactive();
	}

	@Override
	public Connection.Unsafe unsafe() {
		return UNSAFE;
	}

	@Override
	public BungeeServerInfo getInfo() {
		return KeepAliveServerInfo.INFO;
	}

	@Override
	public ChannelWrapper getCh() {
		return this.channel;
	}

	@Override
	public void sendData(String paramString, byte[] paramArrayOfByte) {
	}

	private synchronized void inactive() {
		if (!this.active) {
			return;
		}
		System.out.println("Try to deactive");
		this.getInfo().removePlayer(this.player);
		this.active = false;
		System.out.println("CostumServer deactive");
		this.disconnected();
	}

	public abstract void disconnected();
}
