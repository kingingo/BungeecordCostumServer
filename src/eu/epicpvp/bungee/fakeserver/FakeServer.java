package eu.epicpvp.bungee.fakeserver;

import java.util.HashMap;

import dev.wolveringer.bungeeutil.entity.player.Player;
import eu.epicpvp.bungee.fakeserver.connection.LocalServerConnection;
import eu.epicpvp.bungee.fakeserver.world.World;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

public class FakeServer {
	private static HashMap<Player, FakeServer> server = new HashMap<>();
	private LocalServerConnection connection;
	private Player player;
	private ServerConfiguration config;
	private Plugin pluginInstance;

	public static FakeServer getServer(Player player) {
		return server.get(player);
	}

	public static FakeServer createServer(Plugin pluginInstance, Player player, ServerConfiguration config) {
		FakeServer server = new FakeServer(pluginInstance, player, config);
		FakeServer.server.put(player, server);
		return server;
	}

	public FakeServer(Plugin pluginInstance, Player player, ServerConfiguration config) {
		this.player = player;
		this.pluginInstance = pluginInstance;
		this.config = config;
		this.recreateServer();
	}

	public void switchTo(ServerInfo info) {
		if (info == null) {
			this.player.disconnect();
			return;
		}
		this.player.connect(info);
	}

	public void setWorld(World world) {
		this.config.setWorld(world);
		this.recreateServer();
	}

	private void recreateServer() {
		this.connection = new LocalServerConnection(this.pluginInstance, (UserConnection) (ProxiedPlayer) this.player, this.config) {

			@Override
			public void disconnected() {
				server.remove(FakeServer.this.player);
			}
		};
	}

	public LocalServerConnection getConnection() {
		return this.connection;
	}

	public Player getPlayer() {
		return this.player;
	}

	public ServerConfiguration getConfig() {
		return this.config;
	}

	public Plugin getPluginInstance() {
		return this.pluginInstance;
	}
}

