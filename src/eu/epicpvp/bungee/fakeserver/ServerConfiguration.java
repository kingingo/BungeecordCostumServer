package eu.epicpvp.bungee.fakeserver;

import java.util.List;

import eu.epicpvp.bungee.fakeserver.world.World;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class ServerConfiguration {

	private String title;
	private String subTitle;
	private String actionBar;
	private List<String> chat;
	private List<String> tab;
	private World world;
	private int renderDistance;
}

