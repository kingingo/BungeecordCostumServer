package eu.epicpvp.bungee.fakeserver.bungee;

import java.net.InetSocketAddress;
import java.util.HashMap;

import net.md_5.bungee.BungeeServerInfo;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ServerPing;

public class KeepAliveServerInfo
		extends BungeeServerInfo {
	private static HashMap<String, KeepAliveServerInfo> instances = new HashMap<>();

	public static BungeeServerInfo getServerInfo(String server) {
		if (instances.containsKey(server))
			return instances.get(server);
		return new KeepAliveServerInfo(server);
	}

	public static final KeepAliveServerInfo INFO = new KeepAliveServerInfo("ProxyLobby");

	public KeepAliveServerInfo(String name) {
		super(name, new InetSocketAddress(25565), "error", true);
		instances.put(name, this);
	}

	@Override
	public void ping(Callback<ServerPing> callback, int protocolVersion) {
		callback.done((ServerPing) new ServerPing(), null);
	}

	@Override
	public boolean sendData(String channel, byte[] data, boolean queue) {
		return false;
	}
}

