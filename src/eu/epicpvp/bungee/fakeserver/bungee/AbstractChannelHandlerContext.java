package eu.epicpvp.bungee.fakeserver.bungee;

import java.net.SocketAddress;

import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelProgressivePromise;
import io.netty.channel.ChannelPromise;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.EventExecutor;

public class AbstractChannelHandlerContext
		implements ChannelHandlerContext {
	@Override
    public <T> Attribute<T> attr(AttributeKey<T> paramAttributeKey) {
		return null;
	}

	@Override
    public Channel channel() {
		return null;
	}

	@Override
    public EventExecutor executor() {
		return null;
	}

	@Override
    public String name() {
		return null;
	}

	@Override
    public ChannelHandler handler() {
		return null;
	}

	@Override
    public boolean isRemoved() {
		return false;
	}

	@Override
    public ChannelHandlerContext fireChannelRegistered() {
		return null;
	}

	@Override
    public ChannelHandlerContext fireChannelUnregistered() {
		return null;
	}

	@Override
    public ChannelHandlerContext fireChannelActive() {
		return null;
	}

	@Override
    public ChannelHandlerContext fireChannelInactive() {
		return null;
	}

	@Override
    public ChannelHandlerContext fireExceptionCaught(Throwable paramThrowable) {
		return null;
	}

	@Override
    public ChannelHandlerContext fireUserEventTriggered(Object paramObject) {
		return null;
	}

	@Override
    public ChannelHandlerContext fireChannelRead(Object paramObject) {
		return null;
	}

	@Override
    public ChannelHandlerContext fireChannelReadComplete() {
		return null;
	}

	@Override
    public ChannelHandlerContext fireChannelWritabilityChanged() {
		return null;
	}

	@Override
    public ChannelFuture bind(SocketAddress paramSocketAddress) {
		return null;
	}

	@Override
    public ChannelFuture connect(SocketAddress paramSocketAddress) {
		return null;
	}

	@Override
    public ChannelFuture connect(SocketAddress paramSocketAddress1, SocketAddress paramSocketAddress2) {
		return null;
	}

	@Override
    public ChannelFuture disconnect() {
		return null;
	}

	@Override
    public ChannelFuture close() {
		return null;
	}

	@Override
    public ChannelFuture deregister() {
		return null;
	}

	@Override
    public ChannelFuture bind(SocketAddress paramSocketAddress, ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelFuture connect(SocketAddress paramSocketAddress, ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelFuture connect(SocketAddress paramSocketAddress1, SocketAddress paramSocketAddress2, ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelFuture disconnect(ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelFuture close(ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelFuture deregister(ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelHandlerContext read() {
		return null;
	}

	@Override
    public ChannelFuture write(Object paramObject) {
		return null;
	}

	@Override
    public ChannelFuture write(Object paramObject, ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelHandlerContext flush() {
		return null;
	}

	@Override
    public ChannelFuture writeAndFlush(Object paramObject, ChannelPromise paramChannelPromise) {
		return null;
	}

	@Override
    public ChannelFuture writeAndFlush(Object paramObject) {
		return null;
	}

	@Override
    public ChannelPipeline pipeline() {
		return null;
	}

	@Override
    public ByteBufAllocator alloc() {
		return null;
	}

	@Override
    public ChannelPromise newPromise() {
		return null;
	}

	@Override
    public ChannelProgressivePromise newProgressivePromise() {
		return null;
	}

	@Override
    public ChannelFuture newSucceededFuture() {
		return null;
	}

	@Override
    public ChannelFuture newFailedFuture(Throwable paramThrowable) {
		return null;
	}

	@Override
    public ChannelPromise voidPromise() {
		return null;
	}

	public <T> boolean hasAttr(AttributeKey<T> arg0) {
		return false;
	}
}

