package eu.epicpvp.bungee.fakeserver.packets;

import dev.wolveringer.bungeeutil.packets.Abstract.PacketPlayOut;
import dev.wolveringer.bungeeutil.packets.Packet;
import dev.wolveringer.bungeeutil.version.BigClientVersion;
import dev.wolveringer.nbt.NBTTagCompound;
import dev.wolveringer.packet.PacketDataSerializer;
import eu.epicpvp.bungee.fakeserver.chunk.ByteChunk;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
public class PacketPlayOutMapChunk extends Packet implements PacketPlayOut {

	@Getter
	@Setter
	private ByteBuf buffer; //do not (de-)serialize all the chunk data unneccessarily
	@Getter
	private ByteChunk selection;
	private NBTTagCompound[] nbtTags = new NBTTagCompound[0];

	@Override
	public void read(PacketDataSerializer serializer) {
		buffer = serializer.slice().retain(); //no memory copy
		serializer.skipBytes(serializer.readableBytes()); //mark as read
		/*
		this.selection = new ByteChunk(s.readInt(), s.readInt(), s.readBoolean(), this.getBigVersion() != ClientVersion.BigClientVersion.v1_8 ? s.readVarInt() : s.readShort() & 65535, this.getVersion().getProtocollVersion(), s.readByteArray(), null);
        if (this.getVersion().getProtocollVersion().ordinal() >= 4) {
            int length = s.readVarInt();
            this.nbtTags = new NBTTagCompound[length];
            int i = 0;
            while (i < length) {
                this.nbtTags[i] = s.readNBT();
                ++i;
            }
        }*/
	}

	@Override
	public void write(PacketDataSerializer serializer) {
		if (buffer != null) {
			serializer.writeBytes(buffer);
			buffer.release();
			return;
		}
		serializer.writeInt(this.selection.getLocX());
		serializer.writeInt(this.selection.getLocZ());
		serializer.writeBoolean(this.selection.isBiome());
		if (this.getBigVersion() != BigClientVersion.v1_8) {
			serializer.writeVarInt(this.selection.getBitmask());
		} else {
			serializer.writeShort((int) ((short) (this.selection.getBitmask() & 65535)));
		}
		serializer.writeByteArray(this.selection.getData(this.getVersion().getProtocolVersionEnum()));
		if (this.getVersion().getProtocolVersionEnum().ordinal() >= 4) {
			int nbtLength = this.nbtTags.length;
			serializer.writeVarInt(nbtLength);
			for (NBTTagCompound nbtTag : this.nbtTags) {
				serializer.writeNBT(nbtTag);
			}
		}
	}

	public void setSelection(ByteChunk selection) {
		this.selection = selection;
		this.selection.setBiome(true);
	}

	public PacketPlayOutMapChunk(ByteChunk selection, NBTTagCompound[] nbtTags) {
		this.selection = selection;
		this.nbtTags = nbtTags;
	}
}

