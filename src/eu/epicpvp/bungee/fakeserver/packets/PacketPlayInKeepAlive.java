package eu.epicpvp.bungee.fakeserver.packets;

import dev.wolveringer.bungeeutil.packets.Packet;
import dev.wolveringer.packet.PacketDataSerializer;

public class PacketPlayInKeepAlive extends Packet {
	int id;

	public PacketPlayInKeepAlive() {
		super(0);
	}

	public PacketPlayInKeepAlive(int id) {
		super(0);
		this.id = id;
	}

	@Override
	public void read(PacketDataSerializer serializer) {
		this.id = serializer.readVarInt();
	}

	@Override
	public void write(PacketDataSerializer serializer) {
		serializer.writeVarInt(this.id);
	}

	public int getId() {
		return this.id;
	}
}

